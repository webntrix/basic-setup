<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  All DB related CRUD(Create, Read, Update, Delete) operations are available here.
 * 
 * @author Harishkumar Reddy <reddy.startupindia@gmail.com>
 * 
 */
class Crud extends CI_Model {

    /**
     * Model constructor with CI Model.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Create a new row in the database
     * @param string $table Table name in the database
     * @param array $data insertable data in array format. This array should be in key=>value pair. here field name as key and savable text as value.
     * @return int get back the inserted record ID 
     */
    public function create($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * Create more than one new rows in the database at once
     * @param string $table Table name in the database
     * @param array $dataSet insertable data in array format. This array should be in key=>value pair. here field name as key and savable text as value.
     * @return int get back the last inserted record ID 
     */
    public function create_batch($table, $dataSet) {
        $this->db->insert_batch($table, $dataSet);
        return $this->db->insert_id();
    }

    /**
     * Get data from database.
     * @param string $table Table name in the database.
     * @param array $where (optional) where condition in array format. This array should be in key=>value pair. here field name as key and compare text as value. default is empty.
     * @param array $fields (optional) list of fields which you want to get data specifically. without this list all. default is empty.
     * @param int $start (optional) start value for limit clause. without $limit it wont effect. default is 0.
     * @param int $limit (optional) number of rows part in the limit clause. default is 0.
     * @param array $orderby (optional) order by clause in array format. This array should be in key=>value pair. here field name as key and order(ASC or DESC) as value. default is empty.
     * @param array $groupby (optional) order by clause in simple array format. default is empty.
     * @return stdObjectit is returning the garbage collection for validation purpose and easy to get in deferent modes.
     */
    public function read($table, $where = array(), $fields = array(), $start = 0, $limit = 0, $orderby = array(), $groupby = array()) {
        if (!empty($fields))
            $this->db->select($fields);


        if (!empty($where)) {
            if (is_array($where)) {
                foreach ($where as $key => $value) {
                    if (is_array($value))
                        $this->db->where_in($key, $value);
                    else
                        $this->db->where($key, $value);
                }
            } else {
                $this->db->where_in($where);
            }
        }

        if ($limit > 0)
            $this->db->limit($start, $limit);

        if (!empty($orderby)){
            foreach ($where as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
            

        if (!empty($groupby))
            $this->db->group_by($groupby);

        return $this->db->get($table);
    }

    /**
     * execute the custom query
     * @param string $query custom query
     * @return stdObjectit is returning the garbage collection for validation purpose and easy to get in deferent modes.
     */
    public function runQuery($query) {
        return $this->db->query($query)->result();
    }

    /**
     *  get the number of rows in the table
     * @param string $table Table name in the database.
     * @return stdObject return the count as object format
     */
    public function getNumRows($table) {
        $this->db->select('count(id) as num_rows');
        return $this->db->get($table)->result();
    }

    /**
     * Update data in the table.
     * @param string $table Table name in the database.
     * @param array $where (optional) where condition in array format. This array should be in key=>value pair. here field name as key and compare text as value. default is empty.
     * @param array $data insertable data in array format. This array should be in key=>value pair. here field name as key and savable text as value.
     * @return int returning the affected rows count.
     */
    public function update($table, $where, $data) {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * Delete data from a table.
     * @param string $table Table name in the database.
     * @param array $where (optional) where condition in array format. This array should be in key=>value pair. here field name as key and compare text as value. default is empty.
     * @return boolean return true are false or 1 or 0
     */
    public function delete($table, $where) {
        return $this->db->delete($table, $where);
    }

    /**
     * Get list off fields of a table
     * @param string $table Table name in the database.
     * @param boolean $detailed default TRUE, return the list of fields and those details like data type, length, comment etc..,. if FALSE return list of fields only.
     * @return stdObjectit is returning the garbage collection for validation purpose and easy to get in deferent modes.
     */
    public function getFields($table, $detailed = TRUE) {
        if (!$detailed)
            return $this->db->list_fields($table);
        else
            return $this->db->query('SHOW FIELDS FROM ' . $table);
    }

    /**
     * get list of tables and those details in the DB
     * @return stdObjectit is returning the garbage collection for validation purpose and easy to get in deferent modes.
     */
    public function getTables() {
        return $this->db->query('show table status');
    }

}
