<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *  Custom helper is contain custom functions.
 * 
 * @author Harishkumar Reddy <reddy.startupindia@gmail.com>
 * 
 */


if (!function_exists("printArray")) {

    /**
     * Print the raw formated data and its data type, and continue the execution
     * @param mixed $data printable data
     * @param bool $showDataType (optional) default FALSE: just print the $data. TRUE: Print the $data and it's data type, length.
     */
    function printArray($data, $showDataType = false) {
        echo "<pre>";
        if ($showDataType)
            var_dump($data);
        else
            print_r($data);
        echo "</pre>";
    }

}

if (!function_exists("printExit")) {

    /**
     * Print the raw formated data and its data type, and stop the execution
     * @param mixed $data printable data
     * @param bool $showDataType (optional) default FALSE: just print the $data. TRUE: Print the $data and it's data type, length.
     */
    function printExit($data, $showDataType = false) {
        echo "<pre>";
        if ($showDataType)
            var_dump($data);
        else
            print_r($data);
        echo "</pre>";
        exit;
    }

}
?>

