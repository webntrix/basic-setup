<?php

/**
 * Default controller
 * 
 * @author Harishkumar Reddy <reddy.startupindia@gmail.com>
 * @version 1.0.1
 * @see https://codeigniter.com/user_guide/general/urls.html
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Constructing the controller object
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Default function
     */
    public function index() {
        $this->load->view('welcome_message');
    }

    /**
     *  Test function. 
     * @param string $name pass your name in the url to print the name
     * ex: localhost/basic-setup/welcome/test/Harishkumar
     */
    public function test($name=""){
        echo "<h1>";
        if(empty($name)){
            echo "Pass your name...";
        }else{
            echo "Hello ". $name ."!";
        }
        echo "</h1>";
    }
}
